import sqlite3

class database:
    def __init__(self):
        self.connection = sqlite3.connect('vkbot')
        self.cursor = self.connection.cursor()
        print(self.check_users())
        print(self.check_messages())
        print(self.check_answers())
        print(self.check_commands())
        

    def check_commands(self):
        try:
            self.cursor.execute('SELECT * FROM commands')
            return 'Table COMMANDS already exists'
        except:
            self.cursor.execute('''CREATE TABLE commands (code varchar(2) primary key,
                                command varchar(255), state varchar(2) )
                                ''')
            return 'Table COMMANDS has been created'
        
    def check_users(self):
        try:
            self.cursor.execute('SELECT * FROM users')
            return 'Table USERS already exists'
        except:
            self.cursor.execute('''CREATE TABLE users (user_id varchar(25) primary key,
                                    user_name varchar(25), state varchar(2),
                                    last_message varchar(255) )''')
            return 'Table USERS has been created'

    def check_answers(self):
        try:
            self.cursor.execute('SELECT * FROM answers')
            print( self.cursor.fetchall() )
            return 'Table ANSWERS already exists'
        except:
            self.cursor.execute('''CREATE TABLE answers (temp varchar(255) primary key,
                                    answer varchar(255), attachment boolean)''')
            return 'Table ANSWERS has been created'

    def check_messages(self):
        try:
            self.cursor.execute('SELECT * FROM messages')
            return 'Table MESSAGES already exists'
        except:
            self.cursor.execute('''CREATE TABLE messages (m_id integer primary key AUTOINCREMENT,
                                    m_text varchar(255), user_id varchar(25), command varchar(1))''')
            return 'Table MESSAGES has been created'

    def get_user(self, user_id):
        self.cursor.execute('SELECT * FROM users WHERE user_id={0}'.format(user_id) )
        ans = self.cursor.fetchall()
        if len(ans) == 0:
            return False
        else:
            return ans[0]

    def get_users(self):
        self.cursor.execute('SELECT * FROM users')
        ans = self.cursor.fetchall()
        users = {}
        for user in ans:
            users[ user[0] ] = list(user[1:])
        return users
    
    def add_user(self, user_id, user_name, state=0, last_message=''):
        try:
            self.cursor.execute('INSERT INTO users VALUES ( "{0}", "{1}", "{2}", "{3}" )'.format(user_id, user_name, state, last_message) )
            self.connection.commit()
            return True
        except:
            return False

    def get_ans(self, message):
        ans = self.cursor.execute('SELECT * FROM answers WHERE temp LIKE "{0}"'.format(message))
        ans = self.cursor.fetchall()
        print(ans)
        if len(ans) == 0:
            return False, False
        else:
            return ans[0][1], ans[0][2]

    def get_commands(self, state):
        self.cursor.execute('SELECT * FROM commands WHERE state={0}'.format(state) )
        ans = self.cursor.fetchall()
        if len(ans) == 0:
            return False
        else:
            return ans
        
    def add_ans(self, template, message, attachment):
        try:
            self.cursor.execute('INSERT INTO answers VALUES ( "{0}", "{1}", "{2}" )'.format(template, message, attachment) )
            self.connection.commit()
            return True
        except:
            return False

    def add_command(self, code, command, state):
        try:
            self.cursor.execute('INSERT INTO commands VALUES ( "{0}", "{1}", "{2}" )'.format(code, command, state) )
            self.connection.commit()
            return True
        except:
            return False







            
        
