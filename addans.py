from database import database

def add_answer(temp, ans, attachment, db):
    try:
        db.add_ans(temp, ans, attachment)
        return 'Запись добавлена'
    except:
        return 'Возникла ошибка при добавлении записи'

def add_command(code, command, state, db):
    try:
        db.add_command(code, command, state)
        return 'Запись добавлена'
    except:
        return 'Возникла ошибка при добавлении записи'

    
db = database()
while True:
    print('1. Добавить новый шаблон')
    i = input('Выберите функцию ')
    if i == '1':        
        temp = input('Введите шаблон сообщения ')
        ans = input('Введите ответ на шаблон ')
        attachment = bool(input('Является ли вложением? (1/0) ' ) )
        add_answer(temp, ans, attachment, db)
    elif i == '2':        
        code = input('Введите код команды ')
        command = input('Введите название команды ')
        state = input('Введите состояние пользователя ' ) 
        add_command(code, command, state, db)
    else:
        break



#1. Добавить вышенаписанный код в функцию add_answer
#2. Добавить функцию add_command (3 аргумента)
