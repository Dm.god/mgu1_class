import vk
import random
import database
import time
import weather

class MyBot:
    def __init__(self, token):
        self.session = vk.Session(access_token = token) 
        self.api = vk.API(self.session)
        self.db = database.database()
        self.users = self.db.get_users()
        self.greeting = 'Привет, {0}\nЯ бот-синебот, можешь воспользоваться моими услугами\n'
        print(self.users)
        self.state_functions = { '0' : self.state_0, '1' : self.state_1, '2' : self.state_2, '3' : self.state_3 }
        self.messages_loop()
        
    def state_0(self, args): #Функция приветствия 
        commands = self.db.get_commands(args['user_state']) # Комманды, доступные пользователю
        ans = self.greeting.format(args['username']) # Формируем приветсвие
        for c in commands: #Перебираем  доступные команды
            ans += c[0] + ' ' + c[1] + '\n' #Добавляем команды в ответ
        self.users[ args['user_id'] ][1] = '1' # Перевод в состояние 1
        return [ans, ''] #Возвращаем ответ

    def state_1(self, args): #Обработчик главного оменю
        if args['message'] in ['1', '01', 'Получить погоду']: #Анализ собщения пользователя
            self.users[ args['user_id'] ][1] = '2' # Перевод в состояние 2
            return ['Введите название города', '']
        elif args['message'] in ['2', '02', 'Получить прогноз']: #Анализ собщения пользователя
            self.users[ args['user_id'] ][1] = '3'# Перевод в состояние 3
            return ['Введите название города', '']
        else:
            return ['Нормально пиши', ''] #Негативный вариант (когда бот не распознал сообщение пользователя)

    def state_2(self, args): #Функция для получения погоды в данный момент
        w = weather.getWeatherToday(weather.key, args['message']) #Получаем погоду на сегодня
        if 'current_temp' in w: # Если в переменной W присутствует ключ current_temp
            ans = 'Погода сегодня в {0}: \nТемпература: {1}\nОписание: {2}\n Температура ощущается: {3}' #Создаем шаблон ответа (с пропусками параметров)
            ans = ans.format(args['message'], w['current_temp'], w['description'], w['feels_like']) # Заполняем шаблон
            self.users[ args['user_id'] ][1] = '1' #Переводим в состояние 1
            return [ans, '']
        else:
            return ['Введите город заново', '']

    def state_3(self, args): #Функция для получения прогноза погоды
        w = weather.getWeatherForecast(weather.key, args['message']) #Получаем прогноз (в виде сиска)
        if type(w) == type(list()): # Если ответ является списком (при правильной работе функции)
            forecast = '' #Создаем пустую переменную погоды
            ans = 'Погода {4} в {0}: \nТемпература: {1}\nОписание: {2}\n Температура ощущается: {3}\n' #Создаем шаблон ответа (с пропусками параметров)
            w = w[::4] #Выбираем каждое 4-е значение (Погода каждые 12 часов)
            self.users[ args['user_id'] ][1] = '1' #Переводим пользователя в состояние 1
            for point_w in w: #Перебираем список с информацией о погоде
                forecast += ans.format(args['message'], point_w['current_temp'], point_w['description'], point_w['feels_like'], point_w['timestamp'])# Заполняем шаблон
            return [forecast, '']
        else:
            return ['Введите город заново', '']

    def state_4(self, args): #Свободное общение с пользователем
        if args['message'] in ['Выход', 'Стоп']:
            commands = self.db.get_commands(args['user_state'])
            ans = ''
            for c in commands:
                ans += c[0] + ' ' + c[1] + '\n'
            self.users[ args['user_id'] ][1] = '1'
            return [ans, '']
        else:
            ans, att = self.db.get_ans(args['message'])
            return [ans, att]
            
    def messages_loop(self):
        while True:
            dialogs = self.api.messages.getDialogs(unanswered = 1, v=5.92)
            time.sleep(0.2)
            if dialogs['count'] != 0:
                user_id = dialogs['items'][0]['message']['user_id']
                self.check_users(user_id)
                message = dialogs['items'][0]['message']['body']
                ans = self.edit_message(message, user_id)
                self.send_message(ans, user_id)

    def check_users(self, user_id):
        user = self.db.get_user(user_id)
        if user == False:
            name = self.api.users.get(user_ids=user_id, v=5.92)[0]['first_name']
            self.users[str(user_id)] = [name, '0', '']
            print(self.db.add_user(user_id, name))
 
    def send_message(self, ans, user_id):
        try:
            self.api.messages.send(user_id=user_id, message = ans['message'], attachment = ans['attachment'], v=5.92, random_id=random.randint(0, 1000000))
            return True
        except Exception:
            return False

    def edit_message(self, message, user_id):
        user_state = self.users[str(user_id)][1]
        ans = { 'message' : '', 'attachment' : '' }
        args = {}
        args['username'] = self.users[str(user_id)][0]
        args['user_state'] = user_state
        args['user_id'] = str(user_id)
        args['message'] = message
        if user_state in self.state_functions:
            func_ans = self.state_functions[user_state](args)
            ans['message'] = func_ans[0]
            ans['attachment'] = func_ans[1]
        '''
        if user_state == '0':
            commands = self.db.get_commands(user_state)
            if message == 'Получить погоду':
                w = weather.getWeatherToday( weather.key, 'Москва')
                ans['message'] = 'Сегодня:\n{0}\nТемпература - {1}'.format( w['description'], w['current_temp'])
            elif commands:
                str_commands = self.greeting.format( self.users[str(user_id)][0] )
                for c in commands:
                    str_commands += c[0] + ' ' + c[1] + '\n'
                ans['message'] = str_commands
            else:
                ans['message'] = 'Я ничего не умею'
        '''
        return ans
        

        
bot = MyBot(token = 'f86482a7b1a846a6f56cdc21f19c7bab23273764b53fb3d76ae4f44e97d202d78ec710bf5f62b9bcf04c2')
