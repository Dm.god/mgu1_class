import requests
import datetime

def getWeatherToday(key, city):
    api_url = 'https://api.openweathermap.org/data/2.5/weather?q={0}&appid={1}&lang=RU'
    res = requests.get( api_url.format(city, key) )
    ans = res.json()
    weather = {}
    try:
        weather['current_temp'] = round( ans['main']['temp'] - 273, 1)
        weather['feels_like'] = round( ans['main']['feels_like'] - 273, 1)
        weather['humidity'] = round( ans['main']['humidity'], 1 )
        weather['min_temp'] = round( ans['main']['temp_min'] - 273, 1 )
        weather['max_temp'] = round( ans['main']['temp_max'] - 273, 1 )
        weather['description'] = ans['weather'][0]['description']
    except:
        weather = ans
    return weather
    

def getWeatherForecast(key, city):
    api_url = 'https://api.openweathermap.org/data/2.5/forecast?q={0}&appid={1}&lang=RU'
    res = requests.get( api_url.format(city, key) )
    ans = res.json()
    weather = []
    try:
        for i in ans['list']:
            point_weather = {}
            point_weather['current_temp'] = round( i['main']['temp'] - 273, 1)
            point_weather['feels_like'] = round( i['main']['feels_like'] - 273, 1)
            point_weather['humidity'] = round( i['main']['humidity'], 1 )
            point_weather['min_temp'] = round( i['main']['temp_min'] - 273, 1 )
            point_weather['max_temp'] = round( i['main']['temp_max'] - 273, 1 )
            point_weather['description'] = i['weather'][0]['description']
            point_weather['timestamp'] = datetime.datetime.fromtimestamp(i['dt'])
            print(point_weather['timestamp'])
            weather.append(point_weather)
    except:
        weather = ans
    return weather




key = '5fee3768a934f574254b78789b84225d'
